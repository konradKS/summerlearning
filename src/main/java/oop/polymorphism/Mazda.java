package oop.polymorphism;

public class Mazda extends Car {

    public Mazda(int cylinders) {
        super(cylinders, "Mazda");
    }

    @Override
    public void startEngine() {
        System.out.println(getName() + " start engine ");
    }

    @Override
    public void accelerate(int value) {
        System.out.println(getName() + " speeds up by " + value);

    }

    @Override
    public void breakCar() {
        System.out.println(getName() + " stopped");

    }

    @Override
    public boolean isClima() {
        return true;
    }
}
