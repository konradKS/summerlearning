package oop.polymorphism;

public class Main {

    public static void main(String[] args) {

        Car mazda = new Mazda(8);
        mazda.startEngine();
        mazda.accelerate(45);
        mazda.breakCar();
        System.out.println(mazda.isClima());
        System.out.println(mazda.isEngine());

        System.out.println("================");

        Car opel = new Opel(4);
        opel.startEngine();
        opel.accelerate(2);
        opel.breakCar();
        System.out.println(opel.isClima());
        System.out.println(opel.isEngine());

        System.out.println("================");

        Car hyundai = new Hyundai(4);
        hyundai.startEngine();
        hyundai.accelerate(2);
        hyundai.breakCar();
        System.out.println(hyundai.isClima());
        System.out.println(hyundai.isEngine());

    }

}
