package oop.polymorphism;

public class Opel extends Car {

    public Opel(int cylinders) {
        super(cylinders, "Opel");
        setEngine(false);
    }

    @Override
    public void startEngine() {
        System.out.println(getName() + " start engine ");
    }

    @Override
    public void accelerate(int value) {
        System.out.println(getName() + " speeds up by " + value);

    }

    @Override
    public void breakCar() {
        System.out.println(getName() + " stopped");

    }


}
