package oop.polymorphism;

public class Car {

    private boolean engine;
    private int cylinders;
    private String name;
    private int wheels;

    public Car(int cylinders, String name) {
        this.cylinders = cylinders;
        this.name = name;
        wheels = 4;
        engine = true;
    }

    public void startEngine() {
        System.out.println("CAR starting a engine");
    }

    public void accelerate(int value) {
        System.out.println("The CAR speeds up by " + value);
    }

    public void breakCar() {
        System.out.println("CAR is stopping");
    }

    public boolean isClima() {
        return false;
    }

    public boolean isEngine() {
        return engine;
    }

    public void setEngine(boolean engine) {
        this.engine = engine;
    }

    public int getCylinders() {
        return cylinders;
    }

    public String getName() {
        return name;
    }

    public int getWheels() {
        return wheels;
    }
}
