package oop.polymorphism;

public class Hyundai extends Car {

    public Hyundai(int cylinders) {
        super(cylinders, "Hyundai");
    }

    @Override
    public void startEngine() {
        System.out.println(getName() + " start engine ");
    }

    @Override
    public void accelerate(int value) {
        System.out.println(getName() + " speeds up by " + value);

    }

    @Override
    public void breakCar() {
        System.out.println(getName() + " stopped");

    }

}
