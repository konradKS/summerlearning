package oop.compositionInheritance;

public class Main {

    public static void main(String[] args) {

        Windows windows = new Windows(3);
        Walls walls1 = new Walls("N");
        Walls walls2 = new Walls("S");
        Walls walls3 = new Walls("W");
        Walls walls4 = new Walls("E");
        Doors doors = new Doors(1, "White");

        Room room = new Room(windows, doors, walls1, walls2, walls3, walls4, "red");
        room.makeRoom();
    }


}
