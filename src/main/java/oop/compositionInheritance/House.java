package oop.compositionInheritance;

public class House {

    public Windows windows;
    public Doors doors;
    public Walls walls1;
    public Walls walls2;
    public Walls walls3;
    public Walls walls4;
    public String color;

    public House(Windows windows, Doors doors, Walls walls1, Walls walls2, Walls walls3, Walls walls4, String color) {
        this.windows = windows;
        this.doors = doors;
        this.walls1 = walls1;
        this.walls2 = walls2;
        this.walls3 = walls3;
        this.walls4 = walls4;
        this.color = color;
    }
}
