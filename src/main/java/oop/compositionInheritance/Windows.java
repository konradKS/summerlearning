package oop.compositionInheritance;

public class Windows {

    private int amount;

    public Windows(int amount) {
        this.amount = amount;
    }

    public void makeWindows(){
        System.out.println("Windows are making " + " " + getAmount());
    }

    public int getAmount() {
        return amount;
    }
}
