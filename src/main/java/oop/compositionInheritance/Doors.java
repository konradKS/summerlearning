package oop.compositionInheritance;

public class Doors {

    private int amount;
    private String color;

    public Doors(int amount, String color) {
        this.amount = amount;
        this.color = color;
    }

    public void makeDoors(){
        System.out.println("Doors are making " + getAmount() + " " + getColor());
    }

    public int getAmount() {
        return amount;
    }

    public String getColor() {
        return color;
    }
}
