package oop.compositionInheritance;

public class Room extends House {

    public Room(Windows windows, Doors doors, Walls walls1, Walls walls2, Walls walls3, Walls walls4, String color) {
        super(windows, doors, walls1, walls2, walls3, walls4, color);
    }

    public void makeRoom() {

        walls1.makeWalls();
        walls2.makeWalls();
        walls3.makeWalls();
        walls4.makeWalls();
        doors.makeDoors();
        windows.makeWindows();


    }
}
