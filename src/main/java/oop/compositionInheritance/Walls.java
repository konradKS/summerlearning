package oop.compositionInheritance;

public class Walls {

    private String direction;

    public Walls(String direction) {
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }

    public void makeWalls(){
        System.out.println("Walls are making " + getDirection());
    }
}
