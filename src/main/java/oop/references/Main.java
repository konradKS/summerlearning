package oop.references;

public class Main {

    public static void main(String[] args) {
        House blueHouse = new House("blue");
        House other = blueHouse;

        System.out.println(blueHouse.getColor());
        System.out.println(other.getColor());

        other.setColor("red");

        System.out.println(blueHouse.getColor());
        System.out.println(other.getColor());

        House greenHouse = new House("green");
        other = greenHouse;

        System.out.println(greenHouse.getColor());
        System.out.println(other.getColor());


    }


}
