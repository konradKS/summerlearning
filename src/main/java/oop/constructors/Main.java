package oop.constructors;

public class Main {

    public static void main(String[] args) {

        VipCostumer vipCostumer = new VipCostumer("john",555.55);

        System.out.println(vipCostumer.getName() + " " + vipCostumer.getCreditLimit()
                            + " " + vipCostumer.getEmail());

    }
}
