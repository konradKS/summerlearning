package oop.inheritance;

public class Opel extends Car {

    private boolean radio;
    private boolean clima;

    public Opel(String color, int size, String name, int wheels, boolean radio, boolean clima) {
        super(color, size, name, wheels);
        this.radio = radio;
        this.clima = clima;
    }

    public Opel(String name, String color) {
        this(color, 1, name, 4, true, false);
    }

    @Override
    public void drive() {
        super.drive();
        System.out.println("opel.drive()");
    }

    @Override
    public void changeGear(int gear) {
        //super.changeGear(gear);
        System.out.println("opel.changeGear() " + gear);
    }

    @Override
    public void increaseSpeed(int speed) {
        super.increaseSpeed(speed);
        System.out.println("opel.increaseSpeed() " + speed);

    }

    @Override
    public void stop() {
        super.stop();
        System.out.println("opel.stop() ");

    }

    public boolean isRadio() {
        return radio;
    }

    public boolean isClima() {
        return clima;
    }
}
