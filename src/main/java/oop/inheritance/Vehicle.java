package oop.inheritance;

public class Vehicle {

    private String color;
    private int size;

    public Vehicle(String color, int size) {
        this.color = color;
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public int getSize() {
        return size;
    }
}
