package oop.inheritance;

public class Car extends Vehicle {

    private int wheels;
    private String name;


    public Car(String color, int size, String name, int wheels) {
        super(color, size);
        this.name = name;
        this.wheels = wheels;
    }

    public Car(String name, int wheels) {
        this("Default", 1, name, wheels);
    }

    public void drive() {
        System.out.println("car.drive() ");
    }

    public void changeGear(int gear) {
        System.out.println("car.changeGear() " + gear);
    }

    public void increaseSpeed(int speed) {
        System.out.println("car.increaseSpeed() " + speed);
    }

    public void stop() {
        System.out.println("car.stop() ");

    }

    public int getWheels() {
        return wheels;
    }

    public String getName() {
        return name;
    }
}
