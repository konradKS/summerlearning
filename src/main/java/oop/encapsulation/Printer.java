package oop.encapsulation;

public class Printer {

    private int tonerLevel = 100;
    private int pages = 0;
    private boolean isDuplex = false;

    public Printer(int tonerLevel, boolean isDuplex) {
        if (tonerLevel >= 0 && tonerLevel <= 100) {
            this.tonerLevel = tonerLevel;
        }
        this.isDuplex = isDuplex;
    }

    public void fillUp(int quantity) {
        if (tonerLevel + quantity <= 100) {
            tonerLevel += quantity;
            System.out.println("\nToner is filled! New level of toner is " + tonerLevel);
        } else {
            System.out.println("\nToo much");
        }
    }

    public void printing() {

        if (tonerLevel > 0) {
            if (!isDuplex) {
                pages++;
                tonerLevel -= 5;
            } else {
                pages += 2;
                tonerLevel -= 10;
            }
            System.out.println("\nPrinting ... ... ...");
            System.out.println("Printed pages: " + pages);
            System.out.println("Toner level: " + tonerLevel);
        } else {
            System.out.println("Toner level is empty!!!");

        }
    }
}
