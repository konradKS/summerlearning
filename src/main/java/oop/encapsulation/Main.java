package oop.encapsulation;

public class Main {

    public static void main(String[] args) {

        Printer printer = new Printer(80,false);
        printer.printing();
        printer.printing();
        printer.printing();
        printer.printing();
        printer.fillUp(40);
    }
}