package oop.billsburgers;

public class Hamburger {

    private String breadRoll;
    private String meat;
    private String name;
    private double priceBurger;
    private String additional1;
    private String additional2;
    private String additional3;
    private String additional4;
    private double priceAd1;
    private double priceAd2;
    private double priceAd3;
    private double priceAd4;
    private double totalCost;

    public Hamburger(String name, String breadRoll, String meat, double priceBurger) {
        this.breadRoll = breadRoll;
        this.meat = meat;
        this.priceBurger = priceBurger;
        this.name = name;
        additional1 = "tomato";
        additional2 = "carrot";
        additional3 = "ketchup";
        additional4 = "cheese";
        priceAd1 = 1.99;
        priceAd2 = 1.49;
        priceAd3 = 0.99;
        priceAd4 = 2.99;
        totalCost = 0.0;

    }

    public void pricing() {
        System.out.println("***************************");
        System.out.println("!!!Pricing!!!");
        System.out.println(getName() + " ( " + getBreadRoll() + ", " + getMeat() + " ) - " + getPriceBurger() + "$");
        System.out.println("!!!Additionals!!!");
        System.out.println(additional1 + " - " + getPriceAd1() + "$");
        System.out.println(additional2 + " - " + getPriceAd2() + "$");
        System.out.println(additional3 + " - " + getPriceAd3() + "$");
        System.out.println(additional4 + " - " + getPriceAd4() + "$");
        System.out.println("***************************");
    }

    public double buy() {
        totalCost += getTotalCost() + getPriceBurger();
        System.out.println("\nThank for your shopping!");
        return totalCost;
    }

    public void addAdditions(String name) {

        switch (name) {
            case "tomato":
                totalCost += getPriceAd1();
                break;
            case "carrot":
                totalCost += getPriceAd2();
                break;
            case "ketchup":
                totalCost += getPriceAd3();
                break;
            case "cheese":
                totalCost += getPriceAd4();
                break;
        }
    }

    public String getBreadRoll() {
        return breadRoll;
    }

    public void setBreadRoll(String breadRoll) {
        this.breadRoll = breadRoll;
    }

    public String getMeat() {
        return meat;
    }

    public String getName() {
        return name;
    }

    public double getPriceBurger() {
        return priceBurger;
    }

    public String getAdditional1() {
        return additional1;
    }

    public void setAdditional1(String additional1) {
        this.additional1 = additional1;
    }

    public String getAdditional2() {
        return additional2;
    }

    public void setAdditional2(String additional2) {
        this.additional2 = additional2;
    }

    public String getAdditional3() {
        return additional3;
    }

    public void setAdditional3(String additional3) {
        this.additional3 = additional3;
    }

    public String getAdditional4() {
        return additional4;
    }

    public void setAdditional4(String additional4) {
        this.additional4 = additional4;
    }

    public double getPriceAd1() {
        return priceAd1;
    }

    public double getPriceAd2() {
        return priceAd2;
    }

    public double getPriceAd3() {
        return priceAd3;
    }

    public double getPriceAd4() {
        return priceAd4;
    }

    public double getTotalCost() {
        return totalCost;
    }
}
