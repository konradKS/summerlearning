package oop.billsburgers;

public class HealthyBurger extends Hamburger {

    private String additional1;
    private String additional2;
    private double priceAd1;
    private double priceAd2;
    private double totalCost;


    public HealthyBurger(String name, String meat, double priceBurger) {
        super(name, "Brown rye bread roll", meat, priceBurger);
        additional1 = "olives";
        additional2 = "onion";
        priceAd1 = 3.99;
        priceAd2 = 5.99;
        totalCost = getTotalCost();

    }

    @Override
    public void pricing() {
        super.pricing();
        System.out.println(additional1 + " - " + getPriceAd1() + "$");
        System.out.println(additional2 + " - " + getPriceAd2() + "$");

    }

    @Override
    public double buy() {
        return super.buy() + totalCost;
    }


    @Override
    public void addAdditions(String name) {

        super.addAdditions(name);
        switch (name) {
            case "olivies":
                totalCost += priceAd1;
                break;
            case "onion":
                totalCost += priceAd2;
                break;
        }
    }

    @Override
    public String getAdditional1() {
        return additional1;
    }

    @Override
    public String getAdditional2() {
        return additional2;
    }

    @Override
    public double getPriceAd1() {
        return priceAd1;
    }

    @Override
    public double getPriceAd2() {
        return priceAd2;
    }
}
