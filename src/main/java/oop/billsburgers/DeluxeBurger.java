package oop.billsburgers;

public class DeluxeBurger extends Hamburger {

    private String additions1;
    private String additions2;
    private double priceAd1;
    private double priceAd2;
    private double totalCost;


    public DeluxeBurger(String name, String breadRoll, String meat, double priceBurger) {
        super(name, breadRoll, meat, priceBurger);
        additions1 = "chips";
        additions2 = "drinks";
        priceAd1 = 4.99;
        priceAd2 = 9.99;
        totalCost = getTotalCost();
    }


    @Override
    public void pricing() {
        super.pricing();
        System.out.println(additions1 + " - " + getPriceAd1() + "$");
        System.out.println(additions2 + " - " + getPriceAd2() + "$");
    }

    @Override
    public double buy() {
        return super.buy() + totalCost;
    }

    @Override
    public void addAdditions(String name) {
        super.addAdditions(name);

        switch (name) {
            case "chips":
                totalCost += priceAd1;
                break;
            case "drinks":
                totalCost += priceAd2;
                break;
        }
    }

    public String getAdditions1() {
        return additions1;
    }

    public String getAdditions2() {
        return additions2;
    }

    @Override
    public double getPriceAd1() {
        return priceAd1;
    }

    @Override
    public double getPriceAd2() {
        return priceAd2;
    }
}
