package oop.billsburgers;

public class Main {

    public static void main(String[] args) {

        Hamburger vegetarian = new HealthyBurger("Veg", "Fish", 25.99);
        vegetarian.pricing();
        vegetarian.addAdditions("onion");
        vegetarian.addAdditions("tomato");
        System.out.println("Total cost: " + vegetarian.buy());

        System.out.println("=============");

        Hamburger deluxe = new DeluxeBurger("Deluxe", "White", "Kow", 35.99);
        deluxe.pricing();
        deluxe.addAdditions("drinks");
        System.out.println("Total cost: " + deluxe.buy());
    }
}
