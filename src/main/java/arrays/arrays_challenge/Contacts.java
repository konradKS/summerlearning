package arrays.arrays_challenge;

public class Contacts {

    private String name;
    private String phoneNo;

    public Contacts(String name, String phoneNo) {
        this.name = name;
        this.phoneNo = phoneNo;
    }

    public Contacts() {

    }

    public Contacts createContact(String name, String phoneNo){
        return new Contacts(name,phoneNo);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }
}
