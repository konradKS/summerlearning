package arrays.arrays_challenge;

import java.util.Scanner;

public class Main {
    private static MobilePhone mobilePhone = new MobilePhone();
    private static Main main = new Main();
    private Scanner scanner = new Scanner(System.in);
    private Contacts contacts = new Contacts();

    public static void main(String[] args) {


        boolean isEnd = false;
        int choice = 0;

        main.menu();
        while (!isEnd) {
            System.out.println("\nType a number");
            choice = main.scanner.nextInt();

            switch (choice) {

                case 1:
                    main.menu();
                    break;
                case 2:
                    mobilePhone.printContacts();
                    break;
                case 3:
                    main.addContacts();
                    break;
                case 4:
                    main.updateContacts();
                    break;
                case 5:
                    main.removeContacts();
                    break;
                case 6:
                    main.searchContacts();
                    break;
                case 7:
                    isEnd = true;
            }
        }
    }

    private void searchContacts() {
        if(mobilePhone.getContactsList().isEmpty()){
            System.out.println("The list of contacts is empty.");
            return;
        }

        System.out.println("Enter a name of contact to search");
        String name = scanner.next();
        Contacts contacts = mobilePhone.searchContact(name);

        if (contacts != null) {
            System.out.println(contacts.getName() + " " + contacts.getPhoneNo());
        } else
            System.out.println("Contact not found!");

    }

    private void removeContacts() {
        if(mobilePhone.getContactsList().isEmpty()){
            System.out.println("The list of contacts is empty.");
            return;
        }

        System.out.println("Enter a name of contact to remove");
        String name = scanner.next();

        Contacts contacts = mobilePhone.searchContact(name);

        if (contacts == null) {
            System.out.println("Contact not found");
            return;
        }
        mobilePhone.removeContact(contacts);
    }

    private void updateContacts() {
        if(mobilePhone.getContactsList().isEmpty()){
            System.out.println("The list of contacts is empty.");
            return;
        }

        System.out.println("Enter a name of contact to modify");
        String name = scanner.next();
        int index = mobilePhone.findContact(name);
        if (index >= 0) {
            System.out.println("\nEnter a  new name");
            String newName = scanner.next();

            System.out.println("\nEnter a  new phone number");
            String newPhoneNum = scanner.next();
            contacts = contacts.createContact(newName,newPhoneNum);
            mobilePhone.modifyContacts(index, contacts);
        } else
            System.out.println("!Contact does not exist!");


    }

    private void addContacts() {
        System.out.println("Enter a new contact name: ");
        String name = scanner.next();
        System.out.println("Enter a new phone number: ");
        String phoneNo = scanner.next();
        mobilePhone.addContacts(contacts.createContact(name, phoneNo));
    }

    public void menu() {
        System.out.println("\n1 - printing menu");
        System.out.println("2 - printing contacts");
        System.out.println("3 - add contacts");
        System.out.println("4 - update contacts");
        System.out.println("5 - remove contacts");
        System.out.println("6 - search contacts");
        System.out.println("7 - quit");
    }
}
