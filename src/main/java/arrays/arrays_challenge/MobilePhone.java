package arrays.arrays_challenge;

import java.util.ArrayList;

public class MobilePhone {

    private ArrayList<Contacts> contactsList = new ArrayList<>();

    public void printContacts() {
        for(Contacts contatct : contactsList)
                System.out.println(contatct.getName() + " " + contatct.getPhoneNo());

        if(contactsList.isEmpty()) System.out.println("The list of contacts is empty.");
    }

    public void addContacts(Contacts contacts) {
        if (findContact(contacts.getName()) >= 0)
            System.out.println("Contact is already exist!");
        else
            contactsList.add(contacts);
        System.out.println("Contact " + contacts.getName() + " " + contacts.getPhoneNo() + " added succeed!");
    }

    public void modifyContacts(int index, Contacts contacts) {
        if (index >= 0) {
            contactsList.set(index,contacts);
            System.out.println("Modified succeed!");
        } else {
            System.out.println("Contact does not exist!");
        }
    }

    public void removeContact(Contacts contacts) {
        int index = findContact(contacts.getName());
        if (index >= 0) {
            contactsList.remove(index);
            System.out.println("Contact " + contacts.getName() + " removed!");
        } else {
            System.out.println("Contact does not exist!");
        }
    }


    public int findContact(String name) {
        for (int i = 0; i < contactsList.size(); i++) {
            Contacts contacts = contactsList.get(i);
            if (contacts.getName().equals(name))
                return i;
        }
        return -1;
    }

    public Contacts searchContact(String name) {
        int index = findContact(name);
        if (index >= 0) {
            return contactsList.get(index);
        } else
            return null;
    }

    public ArrayList<Contacts> getContactsList() {
        return contactsList;
    }
}

