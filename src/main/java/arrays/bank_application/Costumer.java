package arrays.bank_application;

import java.util.ArrayList;

public class Costumer {

    private String name;
    private ArrayList<Double> transaction;

    public Costumer(String name, double initialTransaction) {
        this.name = name;
        transaction = new ArrayList<>();
        addTransaction(initialTransaction);
    }

    public Costumer() {
        this("Unknown",0.0);
    }

    public void addTransaction(double amount) {
        transaction.add(amount);
    }

    public Costumer createCostumer(String name, double initialTransaction) {
        return new Costumer(name, initialTransaction);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Double> getTransaction() {
        return transaction;
    }
}
