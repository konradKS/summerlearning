package arrays.bank_application;

import java.util.ArrayList;

public class Branch {

    private String name;
    private ArrayList<Costumer> listOfCostumers;
    private Costumer costumer;

    public Branch(String name) {
        this.name = name;
        listOfCostumers = new ArrayList<>();
        costumer = new Costumer();
    }

    public Branch() {
        this("Unknown");
    }

    public Branch createBranch(String name){
        return new Branch(name);
    }

    public boolean addCostumer(String name, double initialAmount) {
        if (findCostumer(name) == null) {
            listOfCostumers.add(costumer.createCostumer(name, initialAmount));
            return true;
        }
        return false;
    }

    public boolean additionalTransaction(String name, double amount) {
        Costumer costumer = findCostumer(name);
        if (costumer != null) {
            costumer.addTransaction(amount);
            return true;
        }
        return false;
    }

    public Costumer findCostumer(String name) {
        for (int i = 0; i < listOfCostumers.size(); i++) {
            Costumer costumer = listOfCostumers.get(i);
            if (costumer.getName().equals(name))
                return costumer;
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Costumer> getListOfCostumers() {
        return listOfCostumers;
    }
}
