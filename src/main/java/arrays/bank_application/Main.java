package arrays.bank_application;

public class Main {

    public static void main(String[] args) {

        Bank bank = new Bank("Polish bank");

        bank.addBranch("Region");
        bank.addCostumer("Region","Jan",3.52);
        bank.addCostumer("Region","Ola",199.34);
        bank.addTransaction("Region","Jan",33);
        bank.listOfCostumers("Region",true);

    }
}
