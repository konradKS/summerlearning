package arrays.bank_application;

import java.util.ArrayList;

public class Bank {
    private String name;
    private ArrayList<Branch> listOfBranches;
    private Branch branch;

    public Bank(String name) {
        this.name = name;
        listOfBranches = new ArrayList<>();
        branch = new Branch();
    }

    public boolean addBranch(String name) {
        Branch currBranch = findBranch(name);
        if (currBranch == null) {
            listOfBranches.add(branch.createBranch(name));
            return true;
        }
        return false;
    }

    public boolean addCostumer(String branchName, String costumerName, double initialAmount) {
        Branch newBranch = findBranch(branchName);
        if (newBranch != null) {
            return newBranch.addCostumer(costumerName, initialAmount);
        }
        return false;
    }

    public boolean addTransaction(String branchName, String costumerName, double initialAmount) {
        Branch newBranch = findBranch(branchName);
        if (newBranch != null) {
            return newBranch.additionalTransaction(costumerName, initialAmount);
        }
        return false;
    }

    public void listOfCostumers(String branchName, boolean showTransaction) {
        Branch currBrunch = findBranch(branchName);

        if (currBrunch != null) {
            System.out.println("Costumer of " + currBrunch.getName() + " branch.");

            ArrayList<Costumer> costumers = currBrunch.getListOfCostumers();

            for (int i = 0; i < costumers.size(); i++) {
                Costumer branchCostumers = costumers.get(i);
                System.out.println("Costumer: " + branchCostumers.getName());

                if (showTransaction) {
                    ArrayList<Double> transactions = branchCostumers.getTransaction();
                    for (int j = 0; j < transactions.size(); j++) {
                        System.out.println("Amount: " + transactions.get(j));
                    }
                }
            }
        }
    }

    public Branch findBranch(String name) {
        for (int i = 0; i < listOfBranches.size(); i++) {
            Branch currBranch = listOfBranches.get(i);
            if (currBranch.getName().equals(name))
                return currBranch;
        }
        return null;
    }

    public String getName() {
        return name;
    }
}
