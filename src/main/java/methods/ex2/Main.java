package methods.ex2;

public class Main {

    public static void main(String[] args) {

    }

    public static double calcFeetAndInchesToCentimeters(double feet, double inches) {

        if (feet >= 0 && inches >= 0 && inches <= 12) return -1;

        double cm = feet / 0.032808;
        return  cm;

    }
}
