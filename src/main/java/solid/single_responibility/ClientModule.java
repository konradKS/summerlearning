package solid.single_responibility;

public class ClientModule {

    public void hireNewEmployee(Employee employee){
        System.out.println("hireNewEmployee");
    }

    public void terminateEmployee(Employee employee){
        System.out.println("terminateEmployee");
    }

    public void printEmployeeReport(Employee employee){
        System.out.println("printEmployeeReport");
    }

}
