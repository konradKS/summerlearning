package solid.single_responibility;

public class EmployeeDAO {

    private DatabaseConnectionManager databaseConnectionManager;

    public void saveEmployee(Employee employee) {
        System.out.println("saveEmployee");
    }

    public void deleteEmployee(Employee employee) {
        System.out.println("deleteEmployee");
    }

}
