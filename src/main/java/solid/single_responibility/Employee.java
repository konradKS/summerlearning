package solid.single_responibility;

public class Employee {
    private int id;
    private String name;
    private String department;
    private boolean working;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }

    public boolean isWorking() {
        return working;
    }
}
