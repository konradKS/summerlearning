package solid.single_responibility;

public class DatabaseConnectionManager {

    public void getManagerInstance(DatabaseConnectionManager d){
        System.out.println("getManagerInstance");
    }

    public void connect(){
        System.out.println("connect");
    }

    public void getConnectionObject(){
        System.out.println("getConnectionObject");
    }

    public void disconnect(){
        System.out.println("disconnect");
    }
}
