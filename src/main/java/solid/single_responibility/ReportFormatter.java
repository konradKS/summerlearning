package solid.single_responibility;

public class ReportFormatter {

    public String convertObjectToXML(String object) {
        return "convertObjectToXML";
    }

    public String convertObjectToCSV(String object) {
        return "convertObjectToCSV";
    }

    public String getFormattedEmployee() {
        return "getFormattedEmployee";
    }

}
